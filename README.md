# Universal DID SaaS Project Summary

**Note:** This project has been re-named to [GoDiddy.com](https://godiddy.com/).

For more information, please contact:

 * Markus Sabadello (email: [markus@danubetech.com](mailto:markus@danubetech.com), github: [peacekeeper](https://github.com/peacekeeper/))
 * Bernhard Fuchs (email: [bernhard.fuchs@danubetech.com](mailto:bernhard.fuchs@danubetech.com))

## Introduction

### About Danube Tech

Danube Tech is a small startup in Vienna and very engaged in the global SSI community.
We have been working on DIDs, VCs, and related technologies from the very beginning, and
are actively contributing to standardization efforts at W3C, DIF, etc. Even though we
are still relatively small, Our team and list of active projects are constantly growing :)
Visit us at https://danubetech.com/.

### Universal DID SaaS

We are building the **Universal DID Saas**, a hosted platform that allows developers to
create, update, resolve, and deactivate Decentralized Identifiers (DIDs), as well as to
perform several other advanced DID-related operations. This will build directly on the
well-known open-source tools [Universal Resolver](https://uniresolver.io/) and
[Universal Registrar](https://uniregistrar.io/), and integrate
with other SSI community efforts that also work with DIDs. Our business model will be
similar to other classic SaaS offerings.

## Summary

### Business Problem

The problem we will address is that while many developers and businesses in the SSI
ecosystem use Decentralized Identifiers (DIDs) as the core building block of their
architecture, it is actually surprisingly difficult to work with them. The underlying W3C
specifications ([DID Core](https://w3c.github.io/did-core/) and [DID Resolution](https://w3c-ccg.github.io/did-resolution/))
are still evolving, with many open issues. Another challenge is that the number of DID methods is
constantly growing, and that their individual strengths and weaknesses are not clear. At
the same time, it is becoming more important to not be locked-in to any single DID method.
As a result, developers of SSI use cases need to spend a significant amount of time on
understanding the DID layer, when they would rather use their resources on the implementation
of concrete use cases on a higher layer.

### Technical Solution

The Universal DID SaaS will be a hosted platform that makes it easy for developers to work
with DIDs. This platform can be accessed either via a web frontend, or an API. Basic functions
are creating, resolving, updating, and deactivating
DIDs across a wide range of DID methods. Advanced functions will e.g. be key management,
notification of DID-related events to observers via web hooks or protocols such as
DIDComm. The obvious advantage of such a SaaS platform is that DID functionality can be
accessed and integrated with applications without the need to deploy DID infrastructure.

## Integration with the eSSIF-Lab Functional Architecture and Community

See https://gitlab.grnet.gr/essif-lab/business/universal_did_saas/deliverables/-/tree/master/tech_deliverables for:

* Functional Specifications
* API Interface Specifications

Integration of our results with other initiatives is very important to us:

* **eSSIF-Lab:** In the [eSSIF-Lab Functional Architecture](https://essif-lab.pages.grnet.gr/framework/docs/functional-architecture), our solution will integrate mostly with the bottom "SSI Protocols And Crypto Layer". We will ensure that DIDs created and managed with our solution will be usable by other eSSIF-Lab components, and that conversely, DIDs created by other eSSIF-Lab components will be usable with our solution.
* **US-DHS-SVIP:** We are participants in DHS SVIP and have already used the Universal Resolver and Universal Registrar open-source tools to create the DIDs needed for identifying the Issuers in the DHS use cases. Besides us, other participants of the program have also used these tools. One hard requirement is that all participants must support more than one DID method. Going forward, we expect that we will continue to align our technologies with subsequent phases of DHS SVIP.
* **EU-NGI:** The Universal Resolver and Universal Registrar open-source projects were supported by an NGI PET0 grant, and besides funding have also received other support such as an accessibility review and licensing review.
* **Hyperledger Indy/Aries:** The Indy/Aries/Sovrin ecosystem is currently very interested in the "Network-of-Networks" vision, which is closely connected to the new Trust over IP Foundation. There is an ongoing process to upgrade the "sov" DID method into a new "indy" DID method. We will make sure that our solution integrates with new DID specifications and code being developed in that community, e.g. we have contributed [Aries RFC 0124](https://github.com/hyperledger/aries-rfcs/tree/master/features/0124-did-resolution-protocol) that describes one such integration.
* **ESSIF/EBSI**: We have been working on ESSIF in a technical advisor role, and we are also collaborating with the Austrian "e-Government Innovation Center" (eGIZ), which is likely going to operate an EBSI node. Through these relationships, we are planning to add support for EBSI DIDs to our own solutions.
